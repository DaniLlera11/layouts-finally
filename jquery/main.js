console.log("Welcome to jQuery.");

$(document).ready(function(){

  // Creamos una variable button con el contenido de un enlace a google más un salto de línea
  var button = "<a href='http://google.com'>google</a><br/>";

  // La función click del elemento con id shot hace un slideToggle sobre la capa con id ball
  // El slideToggle cambia la visibilidad del elemento con un efecto slide (de visible a no visible)
  $("#shot").click(function() {
    $("#ball").slideToggle();
  });

  // ídem pero con las capas #shot-all y #all
  $("#shot-all").click(function() {
    $("#all").slideToggle();
  });

  // Añade dentro de la div con id info el html <h2>HOLA</h2>
  // Después añade al final de la div el contenido de la variable button varias veces
  // que tiene el html que representa el botón de enlace a google
  $("#info").append("<h2>HOLA</h2>").append(button).append(button).append(button).append(button).append(button);

  // En la función click de la capa info añade un comportamiento
  $("#info").click(function() {
      // Si el color de fondo de la capa es blue (o su representación rgb) entonces cambia el color
      if( $(this).css('background-color') == 'blue' || $(this).css('background-color') == 'rgb(0, 0, 255)') {
      // Cambia el color de fondo a rojo
          $(this).css("background-color", 'red');
      } else { // Si el color no es blue
      // Cambia el color de fondo a blue
      // Cambia la propiedad background-color del css
        $(this).css("background-color", 'blue');
      }
  });

  // Definimos una lista de elementos de tipo String (cadena de textos)
  var marcas = [ "TOYOTA", "AUDI", "MERCEDES" ];

  // Creamos el tag de inico de la lista
  $("#info").append("<ul>");
  // Para cada uno de los elementos de la lista:
  $.each(marcas, function( index, value ) {
    // Añadimos al final de la lista cada elemento
    $("#info ul").append("<li>"+ value+"<li>");
  });
  // Cerramos la lista
  $("#info").append("</ul>");
});
