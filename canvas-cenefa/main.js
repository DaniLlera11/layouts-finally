var c = document.getElementById("mycanvas");
var ctx = c.getContext("2d");
ctx.beginPath();
ctx.strokeStyle = 'green';

ctx.arc(10, 50, 30, 0, 2 * Math.PI);
ctx.arc(10, 50, 20, 0, 2 * Math.PI);

ctx.arc(30, 50, 20, 0, 2 * Math.PI);

ctx.arc(50, 50, 20, 0, 2 * Math.PI);
ctx.arc(70, 60, 20, 0, 5 * Math.PI);
ctx.arc(90, 70, 20, 0, 5 * Math.PI);
ctx.arc(110, 80, 20, 0, 5 * Math.PI);
ctx.arc(130, 90, 20, 0, 5 * Math.PI);
ctx.arc(150, 100, 20, 0, 5 * Math.PI);

ctx.arc(170, 110, 20, 0, 15 * Math.PI);
ctx.arc(170, 110, 30, 0, 15 * Math.PI);

ctx.arc(195, 110, 20, 0, 15 * Math.PI);
ctx.arc(215, 110, 20, 0, 15 * Math.PI);
ctx.arc(235, 90, 20, 0, 15 * Math.PI);
ctx.arc(255, 80, 20, 0, 15 * Math.PI);
ctx.arc(275, 70, 20, 0, 15 * Math.PI);

ctx.arc(295, 60, 20, 0, 15 * Math.PI);
ctx.arc(295, 60, 30, 0, 15 * Math.PI);



ctx.stroke();
  ctx.font = "20px HAPPY COMIC";
  ctx.strokeText("Arte Abstracto",100,30);

var c = document.getElementById("mycanvas2");
var ctx = c.getContext("2d");
ctx.beginPath();
ctx.strokeStyle = '#f94009';
ctx.arc(10, 50, 50, 0, 2 * Math.PI);
ctx.arc(80, 50, 40, 0, 2 * Math.PI);
ctx.arc(140, 50, 30, 0, 2 * Math.PI);
ctx.arc(180, 50, 20, 0, 2 * Math.PI);
ctx.arc(200, 50, 10, 0, 2 * Math.PI);
ctx.arc(210, 50, 5, 0, 2 * Math.PI);
ctx.arc(218, 50, 3, 0, 2 * Math.PI);
ctx.arc(223, 50, 2, 0, 2 * Math.PI);
ctx.arc(225, 50, 2, 0, 2 * Math.PI);
ctx.font = "10px HAPPY COMIC";
ctx.strokeText("()(())()",230,53);
ctx.stroke();
