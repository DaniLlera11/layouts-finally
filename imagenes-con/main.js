$(document).ready(function(){

  console.log("Welcome to jQuery.");

  $("#new-article").click(function(){

    var titulo = $("#form-title").val();
    var description = $("#form-description").val();
    var img = $("#img").val();

    var plantilla = "";
    plantilla += "<article>";
    plantilla += "<h2>";
    plantilla += titulo;
    plantilla += "</h2>";
    plantilla += "<p>";
    plantilla += description;
    plantilla += "</p>";
    plantilla += "</article>";
    plantilla += "<img src='"+img+"'></img>";

    $("#content").append(plantilla);

    // Resetear los valores del formulario
    $("#form-title").val("");
    $("#form-description").val("");

    // Para evitar que la página se recargue;
    return false;

  });

});
