console.log("Welcome to jQuery.");

$(document).ready(function() {

  $(".col10").click(function() {
     // Muestra el texto del div
     //alert($(this).text());
     // Cambia el color de fondo a negro
     //$(this).css("background-color", "");
     // Añade una div flotante dentro de la celda clickada al final del div
     $(this).append($("#palette"));
  });

  $(".colorpicks").click(function() {
    $("#palette").parent().css("background-color", $(this).css("background-color"));
    // Añade una div flotante dentro de la celda clickada al principio del div
    //$(this).prepend('<div class="floating">OLAKEASE</div>');
  });
});
